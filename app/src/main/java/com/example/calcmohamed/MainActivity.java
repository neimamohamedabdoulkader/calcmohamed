package com.example.calcmohamed;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {

        private TextInputEditText operand1EditText;
        private TextInputEditText operand2EditText;
        private Button razButton;
    private TextView errorTextView;
    private Button quitButton;
        private Button calculateButton;
        private RadioGroup operatorRadioGroup;
        private TextView resultTextView;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            operand1EditText = findViewById(R.id.operand1EditText);
            operand2EditText = findViewById(R.id.operand2EditText);
            razButton = findViewById(R.id.razButton);
            errorTextView = findViewById(R.id.errorTextView);
            calculateButton = findViewById(R.id.calculateButton);
            operatorRadioGroup = findViewById(R.id.operatorRadioGroup);
            resultTextView = findViewById(R.id.resultTextView);
            quitButton = findViewById(R.id.quitButton);

            razButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    operand1EditText.getText().clear();
                    operand2EditText.getText().clear();
                    operand1EditText.setHint("Entre un valeur");
                    operand2EditText.setHint("Entre un valeur");
                }
            });

            calculateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Check if an operator is selected
                    if (operatorRadioGroup.getCheckedRadioButtonId() == -1) {
                        // No operator is selected, display the error message
                        errorTextView.setVisibility(View.VISIBLE);
                    } else {
                        // Operator is selected, hide the error message if it was previously displayed
                        errorTextView.setVisibility(View.GONE);

                        // Get the selected operator from the RadioGroup
                        int selectedOperatorId = operatorRadioGroup.getCheckedRadioButtonId();
                        RadioButton selectedOperatorRadioButton = findViewById(selectedOperatorId);

                        // Get the values from the operand EditText fields
                        String operand1Str = operand1EditText.getText().toString();
                        String operand2Str = operand2EditText.getText().toString();

                        // Check if operands are not empty
                        if (!operand1Str.isEmpty() && !operand2Str.isEmpty()) {
                            double operand1 = Double.parseDouble(operand1Str);
                            double operand2 = Double.parseDouble(operand2Str);

                            // Perform the operation based on the selected operator
                            double result = 0.0;
                            String operator = selectedOperatorRadioButton.getText().toString();
                            switch (operator) {
                                case "plus":
                                    result = operand1 + operand2;
                                    break;
                                case "moins":
                                    result = operand1 - operand2;
                                    break;
                                case "multiplie":
                                    result = operand1 * operand2;
                                    break;
                                case "divise":
                                    if (operand2 != 0) {
                                        result = operand1 / operand2;
                                    } else {
                                        resultTextView.setText("Division par zero n'est pas autorise ! ");
                                        return;
                                    }
                                    break;
                            }

                            // Display the result
                            resultTextView.setText("Resultat: " + result);
                        } else {
                            // Display the error message
                            resultTextView.setText("Veuillez entrer des valeurs valides");
                        }
                    }
                }
            });




            quitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    finish();

                }
            });
          
        }
    }



